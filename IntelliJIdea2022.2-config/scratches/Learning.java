cat:
  name: Pelmen
  gender: male
  breed: sphinx
  fur_type: hairless
  fur_pattern: solid
  fur_colors: [ white ]
  tail_length: long
  eyes_colors: [ green ]

  favourite_things:
    - three plaids
    - pile of clothes
    - castle of boxes
    - toys scattered all over the place

  behavior:
    - play:
        condition: boring
        actions:
          - bring one of the favourite toys to the human
          - run all over the house
    - eat:
        condition: want to eat
        actions:
          - shout to the whole house
          - sharpen claws by the sofa
          - wake up a human in the middle of the night
